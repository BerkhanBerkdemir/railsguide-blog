class Comment < ApplicationRecord
  has_many :comments
  validates :title, persense: true, length: { minimum: 5 }
end
